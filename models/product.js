'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.category, { foreignKey: 'category_id' })
    }
  }
  product.init({
    name: {
      type : DataTypes.STRING,
      allowNull : false
    },
    quantity: {
      type : DataTypes.INTEGER,
      allowNull : false
    },
    price: {
      type : DataTypes.FLOAT,
      allowNull : false
    },
    description: {
      type : DataTypes.STRING,
      allowNull : true
    },
    image: {
      type : DataTypes.STRING,
      allowNull : true
    },
    is_active: {
      type : DataTypes.BOOLEAN,
      defaultValue : true
    },
    category_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'product',
    tableName: 'products',
    timestamps: true
  });
  return product;
};