require('dotenv').config() // kegunaan untuk membaca file .env
const express = require('express')
const port = process.env.PORT || 3500 // declare port dari env 
const app = express() // inisiasi function express ke varable app
const cors = require('cors') // inisiasi variabel bersi cors
const bodyParser = require('body-parser')
const router = require('./router')
const YAML = require('yamljs')
const swaggerUI = require('swagger-ui-express')
const apiDocs = YAML.load('./api-doc.yaml')

app.use(cors());
app.use(express.urlencoded({extended : true}))
app.use(bodyParser.json())
app.use('/api', router)
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(apiDocs))


app.listen(port, () => {
    console.log(`server is running on port ${port}`);
})