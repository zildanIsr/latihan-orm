const express = require('express')
const categoryRouter = require('./category')
const userRouter = require('./user')
const router = express.Router()

router.get('/check-healty', (req, res) => res.send("aplication up"))
router.use('/category', categoryRouter)
router.use('/user', userRouter)

module.exports = router 