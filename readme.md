step step 
 - inisiasi project  
   - yarn add cors body-parser express dotenv 
   - yarn add -D nodemon 
 
 - inisiasi sequalize 
   - yarn add sequelize pg pg-hstore 
   - yarn add -D sequelize-cli 
   - npx sequelize-cli init 
   - create file .sequalizerc in root, for content see in repo 
   - change config/config.json to config.js, for content see in repo 
   - add db property to .env 
   - create database in postgress 
   - change model/index.js config.json to config.js ( line 8 ) 
 - create model n relation 
   - npx sequelize model:generate --name {nama model tunggal example category} --attributes {nama_column:tipe_data jika lebih satu di pisahkan dengan ,} 
   - modify model and migration 
   - npx sequelize-cli db:migrate  
 - create crud operation 
   - create route base